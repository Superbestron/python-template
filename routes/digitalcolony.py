import json
import logging

from flask import request

from routes import app

logger = logging.getLogger(__name__)

@app.route('/digital-colony', methods=['POST'])
def evaluateDigitalColony():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = colony(data)
    logging.info("My result :{}".format(result))
    return json.dumps(result)


pair = [[i-j if i>=j else i+10-j for j in range(10)] for i in range(10)]

def colony(json_object):
    global pair
    out = []
    for line in json_object:
        generations = line["generations"]
        colony = line["colony"]
        cpair = [[0 for j in range(10)] for i in range(10)]
        s = [0 for i in range(10)]

        c = [int(i) for i in colony]

        for i in range(len(c)-1):
            cpair[c[i]][c[i+1]] += 1
        for i in range(len(c)):
            s[c[i]] += 1
        tot = 0
        for i in range(51):
            total = sum([s[j]*j for j in range(10)])
            if i == generations:
                tot = total
                break
            tmpair = [[0 for k in range(10)] for j in range(10)]
            mod = total % 10
            for j in range(10):
                for k in range(10):
                    if cpair[j][k] > 0:
                        gen = (pair[j][k] + mod) % 10
                        s[gen] += cpair[j][k]
                        tmpair[j][gen] += cpair[j][k]
                        tmpair[gen][k] += cpair[j][k]
            cpair = tmpair
        out.append(str(tot))
    return out

# f = open('test.json')
# data = json.load(f)
# print(colony(data))
