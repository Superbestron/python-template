import json
import logging

from flask import request, jsonify

from routes import app

logger = logging.getLogger(__name__)


@app.route('/minichess', methods=['POST'])
def evaluateMiniChess():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = agent(data)
    logging.info("My result :{}".format(result))
    return jsonify(result)

class Piece:
    def __init__(self, val, coord, side, hi):
        self.val = val
        self.coord = coord
        self.side = side
        self.hi = hi

    def moves(self): return []


class Knight(Piece):
    def __init__(self, coord, side):
        Piece.__init__(self, 1.3, coord, side, 1)

    def moves(self): return cascade(self.coord, knightX, knightY, False)


class Rook(Piece):
    def __init__(self, coord, side):
        Piece.__init__(self, 1.4, coord, side, 2)

    def moves(self): return cascade(self.coord, horX, horY, True)


class Bishop(Piece):
    def __init__(self, coord, side):
        Piece.__init__(self, 1.2, coord, side, 3)

    def moves(self): return cascade(self.coord, diagX, diagY, True)


class Queen(Piece):
    def __init__(self, coord, side):
        Piece.__init__(self, 1.5, coord, side, 4)

    def moves(self): return cascade(self.coord, allX, allY, True)


class King(Piece):
    def __init__(self, coord, side):
        Piece.__init__(self, int(1e9), coord, side, 5)

    def moves(self): return cascade(self.coord, allX, allY, False)


class Pawn(Piece):
    def __init__(self, coord, side):
        Piece.__init__(self, 1, coord, side, 6)

    def moves(self):
        return pawn_cascade(self.coord)


def pawn_cascade(coord):
    x, y = coord
    side = grid[x][y].side
    pos = []
    if side == -1:
        a = [(x + 1, y - 1), (x + 1, y + 1)]
        m = (x + 1, y)
    else:
        a = [(x - 1, y - 1), (x - 1, y + 1)]
        m = (x - 1, y)
    for atk in a:
        if 0 <= atk[0] < maxX and \
                0 <= atk[1] < maxY and \
                grid[atk[0]][atk[1]].side == -side:
            pos.append(atk)
    if 0 <= m[0] < maxX and \
            0 <= m[1] < maxY and \
            not grid[m[0]][m[1]].side:
        pos.append(m)
    return pos


def cascade(coord, dirX, dirY, cascade):
    coordX, coordY = coord
    side = grid[coordX][coordY].side
    pos = []
    for i in range(len(dirX)):
        step = 1
        while \
                0 <= coordX + dirX[i] * step < maxX and \
                        0 <= coordY + dirY[i] * step < maxY and \
                        (not grid[coordX + dirX[i] * step][coordY + dirY[i] * step].side or \
                         grid[coordX + dirX[i] * step][coordY + dirY[i] * step].side == -side):
            pos.append((coordX + dirX[i] * step, coordY + dirY[i] * step))
            if grid[coordX + dirX[i] * step][coordY + dirY[i] * step].side == -side: break
            if not cascade: break
            step += 1

    return pos


maxX, maxY = 5, 5
allX, allY = [0, 1, 1, 1, 0, -1, -1, -1], [1, 1, 0, -1, -1, -1, 0, 1]
horX, horY = [0, 1, 0, -1], [1, 0, -1, 0]
diagX, diagY = [1, 1, -1, -1], [1, -1, -1, 1]
knightX, knightY = [1, 2, 2, 1, -1, -2, -2, -1], [2, 1, -1, -2, -2, -1, 1, 2]
pieceDict = {
    "\u2659": ("white player", "pawn piece"),
    "\u265F": ("black player", "pawn piece"),
    "\u2656": ("white player", "rook piece"),
    "\u265C": ("black player", "rook piece"),
    "\u2658": ("white player", "knight piece"),
    "\u265E": ("black player", "knight piece"),
    "\u2657": ("white player", "bishop piece"),
    "\u265D": ("black player", "bishop piece"),
    "\u2655": ("white player", "queen piece"),
    "\u265B": ("black player", "queen piece"),
    "\u2654": ("white player", "king piece"),
    "\u265A": ("black player", "king piece"),
}


def move(piece, coord, enemy_set):
    x, y = coord
    saved = grid[x][y]
    if saved.side: enemy_set.remove(saved)
    grid[x][y] = piece
    oldx, oldy = piece.coord
    grid[oldx][oldy] = Piece(0, (oldx, oldy), 0, 0)
    piece.coord = coord
    return saved, (oldx, oldy)

def undo(piece, saved, coord, enemy_set):
    x, y = coord
    grid[x][y] = piece
    piece.coord = coord
    oldx, oldy = saved.coord
    grid[oldx][oldy] = saved
    if saved.side: enemy_set.add(saved)
    return


def a(alpha, beta, score, depth, side):
    if depth == 0: return score, -1
    pieces = players[0] if side == 1 else players[1]
    enemy_pieces = players[1] if side == 1 else players[0]
    best_move = -1
    if depth > 0:
        for piece in list(pieces):
            for coord in piece.moves():
                saved, saved_coord = move(piece, coord, enemy_pieces)
                h = b(alpha, beta, score + saved.val, depth - 1, -side)
                undo(piece, saved, saved_coord, enemy_pieces)
                if best_move == -1: best_move = (piece.coord, coord)
                if h >= beta:
                    return beta, best_move
                if h > alpha:
                    alpha = h
                    best_move = (piece.coord, coord)
    return alpha, best_move


def b(alpha, beta, score, depth, side):
    if depth == 0: return score
    pieces = players[0] if side == 1 else players[1]
    enemy_pieces = players[1] if side == 1 else players[0]
    if depth > 0:
        for piece in list(pieces):
            for coord in piece.moves():
                saved, saved_coord = move(piece, coord, enemy_pieces)
                h, u = a(alpha, beta, score - saved.val, depth - 1, -side)
                undo(piece, saved, saved_coord, enemy_pieces)
                if h <= alpha:
                    return alpha
                if h < beta:
                    beta = h
    return beta


def parseBoard(json_data):
    pieces = {}
    board = json_data["board"]
    for line in board:
        for string in line:
            if string == "":
                continue
            ps = string.split("|")
            color = pieceDict[ps[0]][0].split(" ")[0]
            ptype = pieceDict[ps[0]][1].split(" ")[0]
            pieces.update({(int(ps[1]), int(ps[2])): (ptype, color)})
    return pieces


def agent(json_data):
    global players, grid
    grid = [[Piece(0, (i, j), 0, 0) for j in range(maxX)] for i in range(maxY)]
    white, black = set(), set()
    players = [white, black]

    gameboard = parseBoard(json_data)

    for coord, (ptype, side) in gameboard.items():
        color = 1 if side == 'white' else -1
        if ptype == 'queen': piece = Queen(coord, color)
        if ptype == 'king': piece = King(coord, color)
        if ptype == 'pawn': piece = Pawn(coord, color)
        if ptype == 'rook': piece = Rook(coord, color)
        if ptype == 'bishop': piece = Bishop(coord, color)
        if ptype == 'knight': piece = Knight(coord, color)
        if color == 1:
            white.add(piece)
        else:
            black.add(piece)

    for i in white: grid[i.coord[0]][i.coord[1]] = i
    for i in black: grid[i.coord[0]][i.coord[1]] = i
    alt = 1
    alpha, move = a(-1e10, 1e10, 0, 6, alt)
    return {"move": [move[0][0], move[0][1], move[1][0], move[1][1]]}


# with open('test.json', 'r', encoding='utf-8') as file:
#     f = json.load(file)
#     print(agent(f))