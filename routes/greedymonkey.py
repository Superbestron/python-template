import json
import logging

from flask import request

from routes import app

logger = logging.getLogger(__name__)

from functools import lru_cache


@app.route('/greedymonkey', methods=['POST'])
def evaluateGreedyMonkey():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    w = data['w']
    v = data['v']
    f = data['f']
    result = func(w, v, f)
    logging.info("My result :{}".format(result))
    return json.dumps(result)


def func(w, v, f):
    @lru_cache(maxsize=None)
    def dp(curr_w, curr_v, idx):
        if idx == len(f):
            return 0
        maxi = 0
        if curr_w + f[idx][0] <= w and curr_v + f[idx][1] <= v:
            maxi = max(maxi, f[idx][2] + dp(curr_w + f[idx][0], curr_v + f[idx][1], idx + 1))
        maxi = max(maxi, dp(curr_w, curr_v, idx + 1))
        return maxi

    return dp(0, 0, 0)
