import json
import logging

from flask import request, jsonify
from routes import app


def airport(json_object):
    global passengers, queries
    out = []
    for test_case in json_object:
        cutoff = test_case['cutOffTime']
        queries = 0
        def f(a):
            global queries
            if a < cutoff:
                queries += 1
                return False
            return True
        passengers = list(filter(f,[i for i in test_case['departureTimes']]))
        cpass = passengers.copy()
        cpass.sort()
        idx = { passengers[i]:i for i in range(len(passengers)) }
        for i in range(len(passengers)):
            if passengers[i] != cpass[i]:
                queries += 2
                init = passengers[i]
                passengers[i], passengers[idx[cpass[i]]] = passengers[idx[cpass[i]]], passengers[i]
                idx[init], idx[cpass[i]] = idx[cpass[i]], i
        out.append({"id":test_case['id'],"sortedDepartureTimes":passengers,"numberOfRequests":queries})
    return out

logger = logging.getLogger(__name__)

@app.route('/airport', methods=['POST'])
def evaluateAirport():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = airport(data)
    logging.info("My result :{}".format(result))
    return jsonify(result)