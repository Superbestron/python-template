import json
import logging

from flask import request

from routes import app

logger = logging.getLogger(__name__)

from typing import Dict, List

def getNextProbableWords(classes: List[Dict],
                         statements: List[str]) -> Dict[str, List[str]]:
  probable_words = {}
  classes_dict = {}
  for item in classes:
    for k, v in item.items():
      classes_dict[k] = v

  for statement in statements:
    parts = statement.split('.')
    class_name = parts[0]
    remaining_parts = parts[1:]

    if class_name in classes_dict:
      class_content = classes_dict[class_name]
      # print("class_content", class_content)

      if isinstance(class_content, list):
        probable_words[statement] = class_content
        if remaining_parts[0]:
          ls = []
          sorted_list = sorted(class_content)
          for item in sorted_list:
            if item.startswith(remaining_parts[0]):
              ls.append(item)
          probable_words[statement] = ls

      elif isinstance(class_content, dict):
        if remaining_parts[0]:
          # polymorphic type
          # print("remaining: ", remaining_parts)
          ls = []
          keys = sorted(class_content.keys())
          for key in keys:
            if class_content[key].startswith("List"):
              pass
            elif key.startswith(remaining_parts[0]):
              ls.append(key)
          probable_words[statement] = ls
        else:
          # print("no remaining", class_content)
          probable_words[statement] = sorted(class_content.keys())[:5]
      else:
        probable_words[statement] = []
    else:
      probable_words[statement] = []
    if not probable_words[statement]:
      probable_words[statement] = ['']
    # print("output", probable_words[statement])

  return probable_words


@app.route('/lazy-developer', methods=['POST'])
def evaluateLazyDeveloper():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    classes = data['classes']
    statements = data['statements']
    result = getNextProbableWords(classes, statements)
    logging.info("My result :{}".format(result))
    return json.dumps(result)
