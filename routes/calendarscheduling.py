# import json
# import logging
# import random
# from copy import deepcopy
# from functools import lru_cache
#
# from flask import request
#
# from routes import app
#
# logger = logging.getLogger(__name__)
#
#
# @app.route('/calendar-scheduling', methods=['POST'])
# def evaluateCalendarScheduling():
#     data = request.get_json()
#     logging.info("data sent for evaluation {}".format(data))
#     result = func(data)
#     logging.info("My result :{}".format(result))
#     return json.dumps(result)
#
# class CustomList:
#     def __init__(self, items):
#         self.items = items
#
#     def __getitem__(self, index):
#         return self.items[index]
#
#     def __setitem__(self, index, value):
#         self.items[index] = value
#
#     def __hash__(self):
#         # Convert the list to a tuple before hashing
#         # You can use any hash function you prefer for the tuple
#         return hash(tuple(self.items))
#
# arr = []
# mp = {"monday": 0, "tuesday": 1, "wednesday": 2, "thursday": 3, "friday": 4, "saturday": 5, "sunday": 6}
# mp_back = {0: "monday", 1: "tuesday", 2: "wednesday", 3: "thursday", 4: "friday", 5: "saturday", 6: "sunday"}
# lessons = [set() for j in range(7)]
# best_result = []
# best_earnings = 0
#
#
# memo = [[]]
# randi = [random.randint(1e9,1e12) for i in range(13)]
# def hash(lst):
#     hashvalue = 0
#     for i in range(len(lst)):
#         hashvalue += lst[i] * randi[i]
#     return hashvalue
#
#
# def backtrack(idx, earnings, capacity):
#     global best_result, best_earnings, memo
#     if idx == len(arr):
#         if earnings > best_earnings:
#             best_earnings = earnings
#             best_result = deepcopy(lessons)
#         return earnings
#     hashVal = hash(capacity)
#
#     lesson = arr[idx]
#     lessonRequestId = lesson["lessonRequestId"]
#     duration = lesson["duration"]
#     availableDays = lesson["availableDays"]
#     potentialEarnings = lesson["potentialEarnings"]
#     ans = 0
#     for day in availableDays:
#         if capacity[mp[day]] - duration >= 0:
#             capacity[mp[day]] -= duration
#             lessons[mp[day]].add(lessonRequestId)
#             ans = max(ans, backtrack(idx + 1, capacity, earnings + potentialEarnings))
#             capacity[mp[day]] += duration
#             lessons[mp[day]].remove(lessonRequestId)
#     ans = max(ans, backtrack(idx + 1, capacity, potentialEarnings))
#     return ans
#
#
# def func(json_object):
#     global arr, best_result, memo
#     arr = json_object
#     capacity = CustomList([12 for i in range(7)])
#     memo = [dict() for i in range(len(arr))]
#     t = backtrack(0, 0, capacity)
#     print(t)
#     ans = {}
#     for idx, st in enumerate(best_result):
#         if not st:
#             continue
#         ans[mp_back[idx]] = [lesson for lesson in st]
#     return ans
#
# f = open('test.json')
# data = json.load(f)
# print(func(data))
