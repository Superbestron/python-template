import json
import logging

from flask import request, jsonify
from routes import app

logger = logging.getLogger(__name__)

# How many letters does it have
# When did you see it
# # what does it rhyme with
# what does it make you think of (4,5)

@app.route('/chinese-wall', methods=['GET'])
def evaluateChineseWall():
    # data = request.get_json()
    # logging.info("data sent for evaluation {}".format(data))
    result = {
        "1": "Fluffy",
        "2": "Galactic",
        "3": "Mangoes",
        "4": "Subatomic",
        "5": "Party"
    }
    logging.info("My result :{}".format(result))
    return jsonify(result)

# f = open('test.json')
# data = json.load(f)
# print(func(data))