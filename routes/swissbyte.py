import json
import logging

from flask import request, jsonify
from routes import app

@app.route('/swissbyte', methods=['POST'])
def evaluateSwissbyte():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = func(data)
    logging.info("My result :{}".format(result))
    return jsonify(result)

def func(data):
    ret = {"outcomes": []}
    code = data['code']
    cases = data['cases']

    funcs = []
    variables = set()
    def lambBtr(line):
        def func():

            # print(line)
            tokens = line.split(' ')
            if tokens[1] == "==" or tokens[1] == "!=" or tokens[1] == ">" or tokens[1] == "<":
                return eval(line)
            elif "=" in line:
                variables.add(tokens[0])
            return exec(line.replace("/", "//"), globals())
        return func

    for line in code:
        tokens = line.split(' ')
        if tokens[0] == "if" or tokens[0] == "endif" or tokens[0] == "fail": # if statement
            funcs.append(tokens[0])
            if tokens[0] == "if":
                funcs.append(lambBtr(line[3:]))
        else:
            funcs.append(lambBtr(line))
    for case in cases:
        variables.clear()
        for (k, v) in case.items():
            exec(f"{k} = {v}", globals())
            variables.add(k)
        idx = 0
        problem = False
        while idx < len(funcs):
            if isinstance(funcs[idx], str):
                if funcs[idx] == "if":
                    idx += 1
                    # print(funcs[idx]())
                    if not funcs[idx]():
                        while funcs[idx] != "endif":
                            idx += 1
                elif funcs[idx] == "endif":
                    pass
                else:
                    problem = True
            else:
                funcs[idx]()
            idx += 1
        # print(not problem)
        ans = {}

        ls = list(variables)
        for var in sorted(ls):
            ans[var] = eval(var)

        json = {
            "is_solvable": "false" if problem else "true",
            "variables": ans
        }
        ret["outcomes"].append(json)

        for var in variables:
           exec(f"del {var}", globals())
        #print(a)
    return ret

f = open('test.json')
data = json.load(f)
print(func(data))
