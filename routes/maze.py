import json
import logging

from flask import request, jsonify

from routes import app

logger = logging.getLogger(__name__)

direction = 2
mp = {
    0: (0, 1),  # north
    1: (1, 2),  # east
    2: (2, 1),  # south
    3: (1, 0)  # west
}
dir_str = {
    0: "up", 1: "right", 2: "down", 3: "left"
}

@app.route('/maze', methods=['POST'])
def evaluateMaze():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = func(data)
    logging.info("My result :{}".format(result))
    return jsonify(result)


def func(data):
    global direction, mp, dir_str
    mazeId = data['mazeId']
    nearby = data['nearby']
    mazeWidth = data['mazeWidth']
    step = data['step']
    isPreviousMovementValid = data['isPreviousMovementValid']
    message = data['message']

    if nearby[mp[(direction + 1) % 4][0]][mp[(direction + 1) % 4][1]] != 0:
        direction = (direction + 1) % 4
        # print("turn right")
    elif nearby[mp[direction][0]][mp[direction][1]] != 0:
        # print("forwad")
        pass
    elif nearby[mp[(4 + direction - 1) % 4][0]][mp[(4 + direction - 1) % 4][1]] != 0:
        direction = (4 + direction - 1) % 4
        # print("turn left")
    else:
        direction = (direction + 2) % 4
        # print("uturn")
    return {"playerAction": dir_str[direction]}

# Remember to comment out
# f = open('test.json')
# data = json.load(f)
# print(func(data))