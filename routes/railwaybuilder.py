import json
import logging

from flask import request

from routes import app

logger = logging.getLogger(__name__)

from functools import lru_cache


@app.route('/railway-builder', methods=['POST'])
def evaluateRailwayBuilder():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = func(data)
    logging.info("My result :{}".format(result))
    return json.dumps(result)


def func(data):
    ans = []
    for d in data:
        arr = d.split(', ')
        l = int(arr[0])
        n = int(arr[1])
        pieces = [int(x) for x in arr[2:]]

        @lru_cache(maxsize=None)
        def dp(curr, idx):
            if idx == n:
                if curr == l:
                    return 1
                else:
                    return 0
            sum = 0
            if pieces[idx] + curr <= l:
                sum += dp(curr + pieces[idx], idx)
            sum += dp(curr, idx + 1)
            return sum
        ans.append(dp(0, 0))
    return ans
