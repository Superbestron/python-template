from flask import Flask

app = Flask(__name__)
import routes.square
import routes.lazydeveloper
import routes.greedymonkey
import routes.railwaybuilder
import routes.airport
import routes.digitalcolony
import routes.maze
import routes.swissbyte
import routes.calendarscheduling
import routes.minichess
import routes.chinesewall
import routes.teleportation