import json
import logging
from math import sqrt
from scipy.spatial import cKDTree
from flask import request, jsonify
from routes import app

@app.route('/teleportation', methods=['POST'])
def evaluateTeleportation():
    data = request.get_json()
    logging.info("data sent for evaluation {}".format(data))
    result = teleport(data)
    logging.info("My result :{}".format(result))
    return json.dumps(result)

def teleport(data):
    orbs, portals, loc = data["k"], data["p"], data["q"]
    portals = cKDTree(list(set([(p[0], p[1]) for p in portals])))

    distance = 0
    prev = (0, 0)
    optimiser = []
    for n in loc:
        walk = sqrt(abs(n[0] - prev[0]) ** 2 + abs(n[1] - prev[1]) ** 2)
        tpDistance, tpIdx = portals.query(n)
        distance += walk
        optimiser.append(min(0.0, tpDistance - walk))
        prev = n
    optimiser.sort()
    for i in range(min(len(loc), orbs)):
        distance += optimiser[i]
    return str(round(distance, 2))


# f = open('test.json')
# data = json.load(f)
# print(func(data))
